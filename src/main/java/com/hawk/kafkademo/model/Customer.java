package com.hawk.kafkademo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Customer {

    private int id;
    private String name;
    private int age;
    private LocalDate dateOfBirth;
    private String address;
    private String maritalStatus;

    private PensionProvider pensionProvider;

    public String getDateOfBirth() {
        return dateOfBirth.toString();
    }

}
