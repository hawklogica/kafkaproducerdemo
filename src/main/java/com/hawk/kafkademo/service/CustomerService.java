package com.hawk.kafkademo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hawk.kafkademo.model.Customer;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private final KafkaProducerConfig kafkaProducer;

    public CustomerService(KafkaProducerConfig kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    public void createCustomer(Customer customer) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String s = objectMapper.writeValueAsString(customer);
            kafkaProducer.kafkaTemplate().send("NewCustomerTopic", s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

}
