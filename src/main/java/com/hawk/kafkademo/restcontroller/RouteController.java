package com.hawk.kafkademo.restcontroller;

import com.hawk.kafkademo.service.CustomerService;
import com.hawk.kafkademo.service.KafkaProducerConfig;
import com.hawk.kafkademo.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customer")
public class RouteController {
    Logger logger = LoggerFactory.getLogger(RouteController.class);


    private final CustomerService customerService;

    public RouteController(KafkaProducerConfig kafkaProducerConfig, CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping()
    public String run() {
        return null;
    }

    @PostMapping()
    public ResponseEntity<?> postCustomer(@RequestBody Customer customer) {
        logger.info("Post Customer called");
        customerService.createCustomer(customer);
        return ResponseEntity.ok("OK");
    }


}
