# Kafka producer project

This is a Spring Boot application demoing the capabilities of Apache Kafka

## Requirements

1. Java 8+
2. Zookeeper and kafka on port 9092


## Usage

1. Start the Spring-Boot project
2. Then send this payload to POST localhost:8080/api/customer
```json
{
    "name": "Metal Licca",
    "age": 23,
    "dateOfBirth": "2000-10-10",
    "address": "Some road 12",
    "maritalStatus": "Divorced"
}
```

## Future Improvements

- Find a way to not write both model classes in both projects.
